$(document).ready(function () {
    $(window).scroll(function(){
        var topDistance = $(window).scrollTop();
        $(".parallax").each(function(index){
            var depth = $(this).attr("data-depth");
            var movement = -(topDistance * depth);
            var translate3d = "translate3d(0, " + movement + "px, 0)";
            $(this).css("-webkit-transform", translate3d);
            $(this).css("-moz-transform", translate3d);
            $(this).css("-ms-transform", translate3d);
            $(this).css("-o-transform", translate3d);
            $(this).css("transform", translate3d);
        });
    });

    $("#arrow-down-button").click(function() {
        $([document.documentElement, document.body]).animate({
            scrollTop: $(".story-title-1:first").offset().top
        }, 2000);
    });
});